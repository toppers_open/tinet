/*
 *  TINET (TCP/IP Protocol Stack)
 * 
 *  Copyright (C) 2001-2009 by Dep. of Computer Science and Engineering
 *                   Tomakomai National College of Technology, JAPAN
 *
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次の条件を満たすこと．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: $
 */

#ifndef _TINET_SYS_CONFIG_H_
#define _TINET_SYS_CONFIG_H_

/*
 *  データリンク層 (ネットワークインタフェース) に関する定義
 */

/*
 *  NIC (NE2000 互換) に関する定義
 */

/* 注意: 16 ビットモードは未実装 */
#ifdef IF_ED_CFG_16BIT

#define NUM_IF_ED_TXBUF		2	/* 送信バッファ数			*/

#else	/* of #ifdef IF_ED_CFG_16BIT */

#define NUM_IF_ED_TXBUF		1	/* 送信バッファ数			*/

#endif	/* of #ifdef IF_ED_CFG_16BIT */

#define TMO_IF_ED_GET_NET_BUF	1	/* [ms]、受信用 net_buf 獲得タイムアウト	*/
					/* [s]、 送信タイムアウト			*/
#define TMO_IF_ED_XMIT		(2*IF_TIMER_HZ)

/*#define IF_ED_CFG_ACCEPT_ALL		 マルチキャスト、エラーフレームも受信するときはコメントを外す。*/

/*
 *  イーサネット出力時に、NIC で net_buf を開放する場合に指定する。
 *
 *  注意: 以下の指定は、指定例であり、if_ed では、
 *        開放しないので、以下のコメントを外してはならない。
 */

/*#define ETHER_NIC_CFG_RELEASE_NET_BUF*/

/*
 *  NIC (RTL8019AS) に関する定義
 */

#define ED_BASE_ADDRESS		0x00200000		/* NIC のレジスタベースアドレス */

#define INHNO_IF_ED		IRQ_EXT5		/* IRQ5 */
#define ED_IER			H8IER
#define ED_IER_IP_BIT		H8IER_IRQ5E_BIT
#define ED_IPR			H8IPRA
#define ED_IPR_IP_BIT		H8IPR_IRQ5_BIT

/*
 *  JSP-1.4.2 以降では、割込みハンドラの登録方法が変更され、
 *  割込みプライオリティ・レベルを設定する必要がある。
 */

#define ED_IPM			IPM_LEVEL0

/*  
 * 割込みハンドラ実行中の割込みマスクの値
 * 　　他の割込みをマスクするための設定  
 * 　　自分と同じレベルの割込み要求をブロックするため、
 * 　　上記の割込み要求レベルより１つ高いレベルに設定する。
 */
#if ED_IPM == IPM_LEVEL0
#define if_ed_handler_intmask		IPM_LEVEL1
#elif ED_IPM == IPM_LEVEL1
#define if_ed_handler_intmask		IPM_LEVEL2
#endif 	/* ED_IPM == IPM_LEVEL0 */

#ifndef _MACRO_ONLY

#if TKERNEL_PRVER < 0x1042u	/* JSP-1.4.2 以前 */

/*
 *  JSP-1.4.2 以前では、IPM をエラーが発生しないように定義する。
 */

typedef UB	IPM;

#else	/* of #if TKERNEL_PRVER < 0x1042u */

#define ed_ena_inter(ipm)	chg_ipm(ipm)

#endif	/* of #if TKERNEL_PRVER < 0x1042u */

/*
 *  関数
 */

#if TKERNEL_PRVER < 0x1042u	/* JSP-1.4.2 以前 */

extern void ed_ena_inter (IPM ipm);

#endif	/* of #if TKERNEL_PRVER < 0x1042u */

extern IPM ed_dis_inter (void);
extern void ed_bus_init (void);
extern void ed_inter_init (void);

#endif	/* of #ifndef _MACRO_ONLY */

#endif /* _TINET_SYS_CONFIG_H_ */
