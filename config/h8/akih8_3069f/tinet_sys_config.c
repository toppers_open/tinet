/*
 *  TOPPERS/JSP Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Just Standard Profile Kernel
 * 
 *  Copyright (C) 2000-2004 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2001-2009 by Dep. of Computer Science and Engineering
 *                   Tomakomai National College of Technology, JAPAN
 *
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: $
 */

/*
 *  JSP-1.4.2 以降で変更された割込みハンドラへの対応
 */

#include <s_services.h>
#include <t_services.h>
#include "kernel_id.h"

#include <tinet_defs.h>
#include <tinet_config.h>

#if defined(SUPPORT_ETHER)

#if TKERNEL_PRVER >= 0x1042u	/* JSP-1.4.2 以降 */

#include "h8_sil.h"

/*
 *  割込みプライオリティレベル設定用のデータ構造
 */
static IRC ed_irc = {
	(UB*)ED_IPR,	/* IPRレジスタの番地			*/
	ED_IPR_IP_BIT,	/* IPRレジスタの該当するビット番号	*/
	ED_IPM,		/* 割込みレベル				*/
	};

/*
 * ed_dis_inter -- 割込みを禁止する。
 */

IPM
ed_dis_inter(void)
{
	IPM	ipm;

	syscall(get_ipm(&ipm));
	syscall(chg_ipm(if_ed_handler_intmask));
	return ipm;
	}

/*
 *  ed_bus_init -- ターゲット依存部のバスの初期化
 */

void
ed_bus_init (void)
{
#ifdef INMEM_ONLY

	/* 外部バスを有効にする。*/
	sil_orb_ddr(IO_PORT1,	0x1f);		/* Enable A0 - A4	*/
	sil_orb_ddr(IO_PORT3,	0xff);		/* Enable D8 - D15	*/
	sil_orb_ddr(IO_PORT8,	H8P8DDR_CS1);	/* Enable CS1		*/
	sil_orb_ddr(IO_PORTB,	H8PBDDR_UCAS);	/* Enable UCAS		*/

#endif	/* of #ifdef INMEM_ONLY */
	}

/*
 *  ed_inter_init -- ターゲット依存部の割込みの初期化
 */

void
ed_inter_init (void)
{
	/* 割込み要求時用プライオリティ・レベルを設定する。*/
	define_int_plevel(&(ed_irc));

	/* NIC の割り込みを許可する。*/
	bitset((UB*)ED_IER, ED_IER_IP_BIT);
	}

#endif	/* of #if TKERNEL_PRVER >= 0x1042u */

#endif	/* of #if defined(SUPPORT_ETHER) */
